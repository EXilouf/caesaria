g_config.languages = [
              { lang : "English",     ext : "en", },
              { lang : "Русский",     ext : "ru", talks : ":/audio/wavs_citizen_ru.zip" },
              { lang : "Українська",  ext : "ua", },
              { lang : "Deutsch",     ext : "de", talks : ":/audio/wavs_citizen_de.zip" },
              { lang : "Svenska"    , ext : "sv", },
              { lang : "Español"    , ext : "sp", talks : ":/audio/wavs_citizen_sp.zip" },
              { lang : "Român"      , ext : "ro", },
              { lang : "Français"   , ext : "fr", talks : ":/audio/wavs_citizen_fr.zip" },
              { lang : "Czech"      , ext : "cs", },
              { lang : "Hungarian"  , ext : "hu", },
              { lang : "Italian"    , ext : "it", talks : ":/audio/wavs_citizen_it.zip" },
              { lang : "Polish"     , ext : "pl", },
              { lang : "Suomi"     ,  ext : "fn", },
              { lang : "Português"  , ext : "pr", },
              { lang : "Cрпски"    ,  ext : "sb", },
              { lang : "Korean"     , ext : "kr", font : "HANBatangB.ttf" }
            ]
