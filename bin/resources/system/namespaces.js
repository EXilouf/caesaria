/********************** namespace game **********************************/
var game = {}
game.ui = {}
game.ui.dialogs = {}
game.ui.infobox = {}
game.ui.caesopedia = {}
game.religion = {}
game.religion.pantheon = {}
game.sound = {}

/********************** namespace level **********************************/
var sim = {}

sim.climate = {}
sim.ui = {}
sim.ui.topmenu = {}
sim.ui.topmenu.help = {}
sim.ui.topmenu.file = {}
sim.ui.topmenu.debug = {}
sim.ui.topmenu.advisors = {}
sim.ui.topmenu.options = {}
sim.ui.dialogs = {}
sim.ui.advisors = {}
sim.hotkeys = {}
sim.fastsave = {}
sim.autosave = {}
sim.timescale = {}

/********************** namespace lobby **********************************/
var lobby = {}

lobby.ui = {}
lobby.ui.newgame = {}
lobby.ui.loadgame = {}
lobby.ui.mainmenu = {}
lobby.ui.options = {}
lobby.ui.dialogs = {}
lobby.hotkeys = {}

/********************** namespace splash **********************************/
var splash = {}
