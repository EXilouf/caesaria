g_config.walker.flag = {
  infiniteWait:-1,
  showDebugInfo:1,
  vividly:2,
  showPath:3,
  userFlag:0x80,
  count:0xff
}

g_config.walker.places = {
  plOrigin:0,
  plDestination:1,
  pcCount:2
}
